# RasIP

Rasip is a program programmed in Bash with functions of tracking of IP / HOST and URL. It also allows extraction of public information from both servers and web pages. The menus are simple and you can perform analysis quickly. Developed by OffShell System.

# -> Instrucciones de uso:
    --A continuación explicaré como ejecutar este script en bash.
    1- Descargar o clonar el archivo de Git Hub.
    2- ir a la carpeta o directorio donde se ha descargado o clonado el archivo.
    3- Dar privilegios al programa con "chmod +x"
    4- Ejecutar programa con el comando "./rasip.sh"
    ·El programa se iniciará y podrás disfrutar de el.
# Recuerda que este software esta bajo licencia GPL.
Un saludo y gracias por utilizar Software OffShell System.

# -> Instructions for use
    --Next I will explain how to execute this script in bash.
    1- Download or clone the Git Hub file.
    2- Go to the folder or directory where the file has been downloaded or cloned.
    3- Give privileges to the program with "chmod + x"
    4- Execute program with the command "./rasip.sh"
    ·The program will start and you can enjoy it.
# Remember that this software is under GPL license.
Greetings and thanks for using Software OffShell System.

# OffShell System. Technological soul. Computer passion. Thanks for use our programs.
# Visit the OffShell System Blog: https://offshellsystem.blogspot.com/
# Facebook: https://www.facebook.com/offshell.system
